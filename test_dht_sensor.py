#!/usr/bin/env python
# SPDX-FileCopyrightText: 2021 ladyada for Adafruit Industries
# SPDX-License-Identifier: MIT

import time
import board
import adafruit_dht
import requests

# Domoticz settings
DOMOTICZ_HOST = "192.168.1.62"
DOMOTICZ_DEVICE_IDX = 8  # Fill in your Domoticz device ID
DOMOTICZ_URL = "http://%s:8080" % DOMOTICZ_HOST

# Initial the dht device, with data pin connected to:
dhtDevice = adafruit_dht.DHT11(board.D4)

# you can pass DHT22 use_pulseio=False if you wouldn't like to use pulseio.
# This may be necessary on a Linux single board computer like the Raspberry Pi,
# but it will not work in CircuitPython.
# dhtDevice = adafruit_dht.DHT22(board.D18, use_pulseio=False)

while True:
    try:
        # Print the values to the serial port
        temperature_c = dhtDevice.temperature
        temperature_f = temperature_c * (9 / 5) + 32
        humidity = dhtDevice.humidity
        # print(
        #     "Temp: {:.1f} F / {:.1f} C    Humidity: {}% ".format(
        #         temperature_f, temperature_c, humidity
        #     )
        # )
        if humidity<25:
            HUM_STAT=2          # Dry
        elif humidity<50:
            HUM_STAT=1          # Comfortable
        elif humidity<75:
            HUM_STAT=0          # Normal
        else:
            HUM_STAT=3          # Wet
        # Update device in Domoticz
        url = "%s/json.htm?type=command&param=udevice&idx=%d&nvalue=0&svalue=%.1f;%.1f;HUM_STAT" % \
            (DOMOTICZ_URL, DOMOTICZ_DEVICE_IDX, temperature_c, humidity)
        resp = requests.get(url)

    except RuntimeError as error:
        # Errors happen fairly often, DHT's are hard to read, just keep going
        # print(error.args[0])
        time.sleep(2.0)
        continue
    except Exception as error:
        dhtDevice.exit()
        raise error

    time.sleep(60.0)
