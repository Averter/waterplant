#!/usr/bin/python
import RPi.GPIO as GPIO
import time
 
#GPIO SETUP
# Define the GPIO pin that we have our digital output from our sensor connected to
channel = 21
GPIO.setmode(GPIO.BCM)       # Set our GPIO numbering to BCM
GPIO.setup(channel, GPIO.IN) # Set the GPIO pin to an input
 
def my_callback(channel):
    if GPIO.input(channel):
        print("Water Detected!")
    else:
        print("No Water Detected!")

# This line tells our script to keep an eye on our gpio pin and let us know when
# the pin goes HIGH or LOW
GPIO.add_event_detect(channel, GPIO.BOTH, bouncetime=300)  
# assign function to GPIO PIN, Run function on change
GPIO.add_event_callback(channel, my_callback)  
    
# This is an infinte loop to keep our script running
while True:
    # This line simply tells our script to wait 1 of a second, this is so
    # the script doesnt hog all of the CPU
    time.sleep(1)
