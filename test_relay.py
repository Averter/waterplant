import RPi.GPIO as GPIO
import time

channel = 17

# GPIO setup
GPIO.setmode(GPIO.BCM)
GPIO.setup(channel, GPIO.OUT)


def motor_off(pin):
    GPIO.output(pin, GPIO.HIGH)  # Turn motor off


def motor_on(pin):
    GPIO.output(pin, GPIO.LOW)  # Turn motor on


if __name__ == '__main__':
    try:
        motor_on(channel)
        time.sleep(6)
        motor_off(channel)
        time.sleep(1)
        GPIO.cleanup()
    except KeyboardInterrupt:
        GPIO.cleanup()
